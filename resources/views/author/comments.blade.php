@extends('layouts.admin')

@section('content')
    
<div class="content">
       
        <div class="card">
            <div class="card-header bg-light">
                Normal Table
            </div>

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                            <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Commentar</th>
                                        <th>Create At</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                    @foreach($comments as $comment)
                        <tr>
                            <td>{{ $comment->id }}</td>
                            <td class="text-nowrap"><a href="{{ route('singlePost', $comment->post_id) }}">{{ $comment->post->title }}</a></td>
                            <td>{{ $comment->content}}</td>
                            <td>{{ \Carbon\Carbon::parse($comment->created_at)->diffForHumans()}}</td>
                            
                          
                           
                        </tr>
                    @endforeach
                         </tbody>
                     
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection