@extends('layouts.admin')

@section('content')

<div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-light">
                            New Product
                        </div>
                        @if(Session::has('success'))
                            <div class="alert alert-success">{{ Session::get('success') }}</div>
                        @endif

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>    
                                    @foreach ($errors->all() as $error)
                                        <li>{{$error }}</li>
                                    @endforeach

                                </ul>
                            </div>
                        @endif

                        {{-- enctype let send files  --}}

                        

                        <form action=" {{ route('EditProduct', $product->id ) }} " method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                        <div class="card-body">
                      

                            <div class="row mt-4">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="placeholder-input" class="form-control-label">Title</label>
                                        <input name="title" id="placeholder-input" class="form-control" value="{{$product->title}}">
                                    </div>
                                </div>

                            
                            </div>

                            <div class="row mt-4">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="placeholder-input" class="form-control-label">Thumbnale</label>
                                            <input type="file" name="thumbnail" id="placeholder-input" class="form-control" >
                                        </div>
                                        <img src="{{ asset($product->thumbnail) }}" width="100">
                                    </div>
    
                                
                                </div>

    

                            <div class="row mt-4">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="placeholder-input" class="form-control-label">Price</label>
                                            <input name="price" id="placeholder-input" class="form-control" value="{{$product->price}}" >
                                        </div>
                                    </div>
    
                                
                            </div>


                            <div class="row mt-4">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="textarea">Description</label>
                                        <textarea name="description" id="textarea" class="form-control" rows="6"  >{{$product->description}}</textarea>
                                    </div>
                                    <button class="btn btn-primary" type="submit">Update Product</button>
                                </div>
                              

                            </div>
                        </div>
                    </form>
                    </div>
                </div>
        

        </div>
    </div>

@endsection