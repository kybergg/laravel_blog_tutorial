@extends('layouts.admin')

@section('content')
<div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-light">
                            New Post
                        </div>
                        @if(Session::has('success'))
                            <div class="alert alert-success">{{ Session::get('success') }}</div>
                        @endif

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>    
                                    @foreach ($errors->all() as $error)
                                        <li>{{$error }}</li>
                                    @endforeach

                                </ul>
                            </div>
                        @endif

                        <form action="{{ route('adminUpdatePost', $post->id) }}" method="POST">
                                {{ csrf_field() }}
                        <div class="card-body">
                      

                            <div class="row mt-4">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="placeholder-input" class="form-control-label">Title</label>
                                        <input name="title" id="placeholder-input" class="form-control" value="{{$post->title}}">
                                    </div>
                                </div>

                            
                            </div>


                            <div class="row mt-4">
                              

                               

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="textarea">Content</label>
                                        <textarea name="content" id="textarea" class="form-control" rows="6" >{{ $post->content }}</textarea>
                                    </div>
                                    <button class="btn btn-primary" type="submit">Create Product</button>
                                </div>
                              

                            </div>
                        </div>
                    </form>
                    </div>
                </div>
        

        </div>
    </div>

@endsection