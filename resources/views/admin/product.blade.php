@extends('layouts.admin')

@section('content')
<div class="content">
       
    <div class="card">
        <div class="card-header bg-light">
            Normal Table
        </div>
        <a href="{{ route('newProduct') }}" class="btn btn-primary" >Create new product</a>

        @if(session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
        @endif

        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                        <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Thumbnale</th>
                                    <th>Title</th>
                                    <th>Descriptiom </th>
                                    <th>price</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>
                @foreach( $products as $product)
                    <tr>
                       <td>{{ $product->id }}</td>
                       <td><img src="{{ asset($product->thumbnail) }}" width="100"></td>
                       <td class="text-nowrap"><a href="{{ route('adminSingleProduct', $product->id)}}"></a>{{ $product->title }}</td>
                       <td>{{ $product->description }}</td>
                       <td>{{ $product->price }}</td>
                       <td> <a href="{{route('adminSingleProduct', $product->id)}}" class="btn btn-warning"><i class="icon icon-pencil"></i></a>
                       
                        <button type="button" class='btn btn-danger' data-toggle="modal" data-target="#deleteProduct-{{ $product->id }}" >
                                        
                            X
                            
                        </button> 

                    </td>

                        <div class="modal fade" id="deleteProduct-{{ $product->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  
                                  <h4 class="modal-title" id="myModalLabel">Delete</h4>
                                </div>
                                <div class="modal-body">
                                  Are you sure you want to delete <b>{{$product->title}}</b> post?
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <form method="POST" id="deleteProduct-{{ $product->id }}" action="{{ route('productDelete', $product->id ) }}">
                                    {{ csrf_field() }}
                                  <button type="submit" class="btn btn-primary" >Save changes</button>
                                </form>
                                </div>
                              </div>
                            </div>
                          </div>


                    </tr>
                @endforeach
                     </tbody>
                 
                </table>
            </div>
        </div>
    </div>
</div>




@endsection