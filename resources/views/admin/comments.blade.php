@extends('layouts.admin')

@section('content')
    
<div class="content">
       
        <div class="card">
            <div class="card-header bg-light">
                Normal Table
            </div>

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                            <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Commentar</th>
                                        <th>Create At</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                    @foreach($comments as $comment)
                        <tr>
                            <td>{{ $comment->id }}</td>
                            <td class="text-nowrap"><a href="{{ route('singlePost', $comment->post_id) }}">{{ $comment->post->title }}</a></td>
                            <td>{{ $comment->content}}</td>
                            <td>{{ \Carbon\Carbon::parse($comment->created_at)->diffForHumans()}}</td>
                            <td>
                            

                            <button type="button" class='btn btn-danger' data-toggle="modal" data-target="#adminDeleteComment-{{$comment->id}}">X</button>
                       
                            <div class="modal fade" id="adminDeleteComment-{{$comment->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      
                                      <h4 class="modal-title" id="myModalLabel">Delete</h4>
                                    </div>
                                    <div class="modal-body">
                                      Are you sure you want to delete <b>{{$comment->post->title}}</b> comment?
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <form method="POST" id="adminDeleteComment-{{$comment->id}}" action="{{ route('adminDeleteComments', $comment->id ) }}">
                                        {{ csrf_field() }}
                                      <button type="submit" class="btn btn-primary" >Save changes</button>
                                    </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                       
                       
                        </td>
                           
                        </tr>
                    @endforeach
                         </tbody>
                     
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection