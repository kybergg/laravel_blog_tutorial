@extends('layouts.admin')

@section('content')
    
<div class="content">
       
        <div class="card">
            <div class="card-header bg-light">
                Normal Table
            </div>

            @if(session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
            @endif
            
            @if(session('error'))
            <div class="alert alert-danger">
                {{session('error')}}
            </div>
            @endif

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                            <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Posts</th>
                                        <th>Comments</th>
                                        <th>Create At</th>
                                        <th>Update At</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                    @foreach( Auth::user()->all() as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td> {{$user->email }}</td>
                            <td> {{$user->posts->count() }}</td>
                            <td> {{$user->comments->count() }}</td>
                            <td> {{ \Carbon\Carbon::parse($user->created_at)->diffForHumans() }}</td>
                            <td> {{ \Carbon\Carbon::parse($user->Update_at)->diffForHumans() }}</td>

                           @if ($user->admin == 0)
                                
                           
                            <td>
                                    <a href="{{ route('editAdminUser', $user->id) }}" class="btn btn-warning"><i class="icon icon-pencil"></i></a>
                                    <form style="display: none;" id="deleteComment-{{$user->id}}" action="{{ route('adminUserDelete', $user->id ) }}" method="POST">
                                            {{ csrf_field() }}
                                        </form>
                                        <button type="button" class='btn btn-danger' data-toggle="modal" data-target="#deleteUser-{{$user->id}}" >
                                            
                                        X
                                        
                                    </button> 

                                    <div class="modal fade" id="deleteUser-{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              
                                              <h4 class="modal-title" id="myModalLabel">Delete</h4>
                                            </div>
                                            <div class="modal-body">
                                              Are you sure you want to delete <b>{{$user->name}}</b> user?
                                            </div>
                                            <div class="modal-footer">
                                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <form method="POST" id="deleteUser-{{$user->id}}" action="{{ route('adminUserDelete', $user->id ) }}">
                                                {{ csrf_field() }}
                                              <button type="submit" class="btn btn-primary" >Save changes</button>
                                            </form>
                                            </div>
                                          </div>
                                        </div>
                                      </div>

                                
                                
                               
                                
                                </td>
                            @else
                            <td></td>
                            @endif 
                           
                        </tr>
                    @endforeach
                         </tbody>
                     
                    </table>
            
                </div>
            </div>
        </div>
    </div>

@endsection