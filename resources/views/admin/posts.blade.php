@extends('layouts.admin')

@section('content')
<div class="content">
       
    <div class="card">
        <div class="card-header bg-light">
            Normal Table
        </div>

        @if(session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
        @endif

        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                        <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Create At</th>
                                    <th>Updated At</th>
                                    <th>Comments</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>
                @foreach(Auth::user()->posts as $post)
                    <tr>
                        <td>{{ $post->id }}</td>
                        <td class="text-nowrap"><a href="{{ route('singlePost', $post->id) }}">{{ $post->title }}</a></td>
                        <td>{{ \Carbon\Carbon::parse($post->created_at)->diffForHumans()}}</td>
                        <td>{{ \Carbon\Carbon::parse($post->updated_at)->diffForHumans()}}</td>
                        <th>{{ $post->comments->count() }} </th>
                        <td>
                            <a href="{{route('adminEditPost', $post->id)}}" class="btn btn-warning"><i class="icon icon-pencil"></i></a>
                            
                            
                            <button type="button" class='btn btn-danger' data-toggle="modal" data-target="#deleteUserPost-{{ $post->id }}" >
                                            
                                X
                                
                            </button> 

                            <div class="modal fade" id="deleteUserPost-{{ $post->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      
                                      <h4 class="modal-title" id="myModalLabel">Delete</h4>
                                    </div>
                                    <div class="modal-body">
                                      Are you sure you want to delete <b>{{$post->title}}</b> post?
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <form method="POST" id="deleteUserPost-{{ $post->id }}" action="{{ route('adminDeletePost', $post->id ) }}">
                                        {{ csrf_field() }}
                                      <button type="submit" class="btn btn-primary" >Save changes</button>
                                    </form>
                                    </div>
                                  </div>
                                </div>
                              </div>


                        </td>

                       
                    </tr>
                @endforeach
                     </tbody>
                 
                </table>
            </div>
        </div>
    </div>
</div>




@endsection