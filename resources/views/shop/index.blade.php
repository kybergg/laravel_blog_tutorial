@extends('layouts.master')

@section('content')
<header class="masthead" style="background-image: url('img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>Shirts shop</h1>
            <span class="subheading">Supper cool t-shirts</span>
          </div>
        </div>
      </div>
    </div>
  </header>

   <!-- Main Content -->
   <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          @foreach ( $products as $product )
          <div class="row">
            <div class="col-md-3">
              <img src="{{ asset($product->thumbnail) }}" width="100">
            </div>
              <div class="col-md-9">
              <div class="post-preview">
                <a href="{{route('shop.singleProduct', $product->id)}}">
                  <h2 class="post-title">
                    {{$product->title}}
                  </h2>
                  <h3 class="post-subtitle">
                      {{$product->description}}
                  </h3>
                </a>
                <p class="post-meta">Price 
                  {{ $product->price }} EUR
              
              </div>
            </div>
          </div>
          <hr>
          @endforeach
          <!-- Pager -->
          {{-- php artisan vendor:publish --tag=laravel-pagination --}}
          {{ $products->links('vendor.pagination.bootstrap-4') }} 

        </div>
      </div>
    </div>

    <hr>
@endsection