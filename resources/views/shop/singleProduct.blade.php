@extends('layouts.master')

@section('content')
<header class="masthead" style="background-image: url('img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>{{$product->title}}</h1>
            <span class="subheading">Supper cool t-shirts</span>
          </div>
        </div>
      </div>
    </div>
  </header>

     <div class="container">
         <div class="row">
            <div class="col-md-6">
                <img src="{{ asset($product->thumbnail) }}" >
            </div>
            <div class="col-md-6">
                <h2>{{ $product->title }}</h2>
                <hr>
                <p>{{ $product->description }}</p>
                <hr>
                <b>{{ $product->price }} EUR</b>
                <br>
                <a href="{{route('shop.productOrder', $product->id ) }}" class="btn btn-primary"> Checkout with PayPal</a>
            </div>
        </div>
     </div>
@endsection