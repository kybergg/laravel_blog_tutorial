<?php

namespace App\Http\Controllers;

use App\Comments;
use Illuminate\Http\Request;
use App\Http\Requests\UpdatePost;
use App\Http\Requests\UserUpdate;
use Illuminate\Support\Carbon;
use App\Post;
use App\User;
use App\Product;
use App\Charts\DashboardChart;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('CheckRole:admin');
        $this->middleware('auth');
    }
    public function dashboard()
    {
        $chart = New DashboardChart;

        $days = $this->generateDataRange(Carbon::now()->subDay('30'), Carbon::now());

        $posts = [];

        foreach($days as $day)
        {
            $posts[] = Comments::whereDate('created_at', $day)->count();
        }

        $chart->dataset('Posts', 'line', $posts);
        $chart->labels($days);
        

        return view('admin.dashboard', compact('chart'));
    }

    private function generateDataRange(Carbon $start_date, Carbon $end_date)
    {
        $dates = [];

        for($date = $start_date; $date->lte($end_date); $date->addDay())
        {
            $dates[] = $date->format('Y-m-d');

        }

        return $dates;
    }

    public function posts()
    {
        $posts = Post::all();
        return view('admin.posts', compact('posts'));
    }

    public function editPost($id)
    {
        $post = Post::where('id', $id)->first();

        return view('admin.editPost', compact('post'));
    }

    public function updatePost(UpdatePost $request, $id)
    {
        $post = Post::where('id', $id)->first();
        $post->title = $request['title'];
        $post->content = $request['content'];
        $post->save();

        return back()->with('success', 'Post is updated');

    }

    public function deletePost($id)
    {
        $post = Post::where('id', $id)->first();
        $comments = Comments::where('post_id', $post->id)->delete();
        $post->delete();

        return back()->with('success', 'Post is deleted');
    }

    public function comments()
    {
        $comments = Comments::all();
        return view('admin.comments', compact('comments'));

    }

    public function deleteComments($id)
    {
        $comments = Comments::where('id', $id)->delete();

        return back()->with('success', "Comment is deleted");
    }

    public function users()
    {

        return view('admin.users');
    }

    public function userDelete($id)
    {
        $user = DB::table('users')->where('id', $id);

        if ($user->first()->admin == 1) {
            return back()->with('error', 'User is admin you cant delete it');

        }

        $user->delete();

        return back()->with('success', 'User is deleted');
    }

    public function editAdminUser($id)
    {
        $user = User::where('id', $id)->first();

         if($user->admin == 1)
         {
             return redirect( route('adminUsers'));
         }

        return view('admin.editAdminUser', compact('user'));
    }

    public function updateAdminUsers(UserUpdate $request, $id)
    {
        $user = User::where('id', $id)->first();
        $user->name = $request['name'];
        $user->email = $request['email'];

        if ($request['author'] == 1 || $request['admin'] == 1) {
            $user->author = true;
            $user->admin = true;
        } elseif ($request['author'] == 1) {
            $user->author = true;
        } elseif ($request['admin'] == 1) {
            $user->admin = true;

        }

        if ($request['author'] == 0 || $request['admin'] == 0) {
            $user->author = false;
            $user->admin = false;
        } elseif ($request['author'] == 0) {
            $user->author = false;
        } elseif ($request['admin'] == 0) {
            $user->admin = false;
        }

        $user->save();

        return back()->with('success', 'User updated');
    }

    // Shop
    public function products()
    {
        $products = Product::all();

        return view('admin.product', compact('products'));
    }

   

    public function newProducts()
    {
        return view('admin.newProduct');
    }

    public function CreateNewProduct(Request $request)
    {
         $this->validate($request, [
             'title' => 'required|string',
             'thumbnail' => 'required|file',
             'price' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)$/',
            'description' => 'required'
         ]);
         $product = new Product;
         $product->title = $request['title'];
         $product->description = $request['description'];
         $product->price = $request['price'];
            // File
         $thumbnail = $request->file('thumbnail');

         $fileName = $thumbnail->getClientOriginalName();
         $fileExtension = $thumbnail->getClientOriginalExtension();
        
         $thumbnail->move('product-img', $fileName);
         $product->thumbnail = 'product-img/' . $fileName;
         
         $product->save();

         return back()->with('success', 'Product created');
    }

    public function singleProduct($id)
    {
        $product = Product::findOrFail($id);
        return view('admin.singleProduct', compact('product'));
    }

    public function editProduct(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|string',
            'thumbnail' => 'file',
            'price' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)$/',
           'description' => 'required'
        ]);

        $product = Product::findOrFail($id);
       
        $product->title = $request['title'];
        $product->description = $request['description'];
        $product->price = $request['price'];
           // File
           if($request->hasFile('thumbnail'))
           {
                $thumbnail = $request->file('thumbnail');

                $fileName = $thumbnail->getClientOriginalName();
                $fileExtension = $thumbnail->getClientOriginalExtension();
            
                $thumbnail->move('product-img', $fileName);
                $product->thumbnail = 'product-img/' . $fileName;
             }
        $product->save();



        return back()->with('success', 'Product whas updated');
    }

    public function productDelete($id)
    {
        $product = Product::findOrFail($id);
        
        // Find path
        $path = public_path() . '\\' .  $product->thumbnail;
        $fullPath = str_replace('/', '\\', $path);

        if(file_exists($fullPath))
        {
            unlink($fullPath);
        }

        $product->delete();
        return back()->with('success', 'Product delete');
    }

}



