<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserUpdate;
use App\Charts\DashboardChart;
use Illuminate\Support\Carbon;
use App\Post;
use App\Comments;


class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function dashboard()
    {
        $chart = New DashboardChart;
        
        $days = $this->generateDataRange(Carbon::now()->subDay('30'), Carbon::now());

        $comments = [];

        foreach($days as $day)
        {
            $comments[] = Comments::whereDate('created_at', $day)->where('user_id', Auth::id())->count();
        }

        $chart->dataset('comments', 'line', $comments);
        $chart->labels($days);
        
        

        return view('user.dashboard',  compact('chart'));
    }

    private function generateDataRange(Carbon $start_date, Carbon $end_date)
    {
        $dates = [];

        for($date = $start_date; $date->lte($end_date); $date->addDay())
        {
            $dates[] = $date->format('Y-m-d');

        }

        return $dates;
    }
    public function comments()
    {


       
        return view('user.comments');
    }

    public function deleteComments($id)
    {
        $comment = Comments::where('id', $id)->where('user_id', Auth::id())->delete();
       
        return back();
    }
    
    public function profile()
    {
        return view('user.profile');
    }

    public function profilePost(UserUpdate $request)
    {
        $user = Auth::user();
        $user->name = $request['name'];
        $user->email = $request['email'];
        $user->save();

        if($request['password'] != "")
        {
            if(!(Hash::check($request['password'],  Auth::user()->password )))
            {
                return redirect()->back()->with('error','Curent password not mach');

            }
            if(strcmp($request['password'], $request['new_password'] ) == 0) 
            {
                return redirect()->back()->with('error', 'Password can be the same as your current password');
            }

           $validation = request()->validate([
             'password' => 'required',
             'new_password' => 'required|string|min:6|confirmed'
           ]);
           
            $user->password = bcrypt($request['new_password']);

            
            $user->save();

            return redirect()->back()->with('success', 'Password change successfully');
        }

       
        
        return back();
    }

    // Add comment
    public function newComment(Request $request)
    {
        $comment = new Comments;
        $comment->content = $request['comment'];
        $comment->user_id = Auth::id();
        $comment->post_id = $request['post'];
        
        $validation = request()->validate([
            'comment' => 'required',
            'post' => 'required'
          ]);


       
        $comment->save();
        
        return back();
    }
}
