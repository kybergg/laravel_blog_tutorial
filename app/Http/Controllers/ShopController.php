<?php

// php artisan make:controller ShopController

namespace App\Http\Controllers;

use App\Product;
use App\Facade\PayPal;
use App\Mail\SendMailPurchase;
use Illuminate\Support\Facades\Mail;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\PaymentExecution;

class ShopController extends Controller
{
    public function index()
    {
        $products = Product::paginate(2);

        return view('shop.index', compact('products'));
    }

    public function SingleProduct($id)
    {
        $product = Product::FindOrFail($id);
        return view('shop.singleProduct', compact('product'));
    }

    public function productOrder($id)
    {
        // Create FacadePayPal.php file
        // Intall Paypal_SDK-php with composer
        // Create Paypal sandbox (fake)account in dev.paypal

        $product = Product::FindOrFail($id);
        $apiContext = PayPal::ApiContext();

        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        $item1 = new Item();
        $item1->setName('Testas 1')
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setSku($product->id) // Similar to `item_number` in Classic API
            ->setPrice($product->price); //35
       

        $itemList = new ItemList();
        $itemList->setItems(array($item1));

        $details = new Details();
        $details->setShipping(2)
            ->setTax(2)
            ->setSubtotal($product->price);

        $amount = new Amount();
        $amount->setCurrency("USD")
            ->setTotal($product->price + 4)
            ->setDetails($details);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("Payment description")
            ->setInvoiceNumber(uniqid());

        $baseUrl = "http://blog.test";
        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl( route('shop.execute', $id))
            ->setCancelUrl(route('shopIndex'));

        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

        $request = clone $payment;

        try {
            $payment->create($apiContext);
        } catch (\Exception $ex) {
            // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
            print("Created Payment Using PayPal. Please visit the URL to Approve." . $request . $ex);
            exit(1);
        }

        $approvalUrl = $payment->getApprovalLink();

       
        return redirect( $approvalUrl);
    }

    public function execute($id)
    {
        $product = Product::FindOrFail($id);
       $apiContext = PayPal::apiContext();

      

            // Get the payment Object by passing paymentId
            // payment id was previously stored in session in
            // CreatePaymentUsingPayPal.php
            $paymentId = $_GET['paymentId'];
            $payment = Payment::get($paymentId, $apiContext);
        
            // ### Payment Execute
            // PaymentExecution object includes information necessary
            // to execute a PayPal account payment.
            // The payer_id is added to the request query parameters
            // when the user is redirected from paypal back to your site
            $execution = new PaymentExecution();
            $execution->setPayerId($_GET['PayerID']);
        
            // ### Optional Changes to Amount
            // If you wish to update the amount that you wish to charge the customer,
            // based on the shipping address or any other reason, you could
            // do that by passing the transaction object with just `amount` field in it.
            // Here is the example on how we changed the shipping to $1 more than before.
            $transaction = new Transaction();
            $amount = new Amount();
            $details = new Details();
        
            $details->setShipping(2)
                ->setTax(2)
                ->setSubtotal($product->price);
        
            $amount->setCurrency('USD');
            $amount->setTotal($product->price + 4);
            $amount->setDetails($details);
            $transaction->setAmount($amount);
        
            // Add the above transaction object inside our Execution object.
            $execution->addTransaction($transaction);
        
            try {
                // Execute the payment
                // (See bootstrap.php for more on `ApiContext`)
                $result = $payment->execute($execution, $apiContext);

              
        
                // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
                print("Executed Payment1". "Payment". $payment->getId() . $execution . $result);
        
                try {
                    $payment = Payment::get($paymentId, $apiContext);
                    $paymentInfo = json_decode($payment);
                    Mail::to($paymentInfo->payer->payer_info->email)
                        ->bcc('test@blog.test')
                        ->send( new SendMailPurchase($paymentInfo));
                    // dd($paymentInfo);
                    // die;
                } catch (\Exception $ex) {
                    // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
                    // print("Get Payment 1 ");
                    // echo($ex);
                    // exit(1);
                    return redirect(route('shopIndex'));
                }
            } catch (\Exception $ex) {
                // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
                // print("Executed Payment2");
                // echo($ex);
                // exit(1);
                return redirect(route('shopIndex'));
            }
        
            // print("Get Payment". "Payment" . $payment->getId());
        
            // return $payment;

            return redirect(route('shopIndex'));
     
    }
}
