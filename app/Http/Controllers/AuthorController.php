<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Comments;
use App\Http\Requests\CreatePost;
use App\Http\Requests\UpdatePost;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Charts\DashboardChart;

class AuthorController extends Controller
{

    public function __construct() {
        $this->middleware('CheckRole:author');
        $this->middleware('auth');
    }

    public function dashboard()
    {
        $posts = Post::where('user_id', Auth::id())->pluck('id')->toArray();
        
        $allComments = Comments::whereIn('post_id', $posts)->get();
        
        $todayComments = $allComments->where('created_at', '>=' , Carbon::today())->count();

        // Chart

        $chart = New DashboardChart;

        $days = $this->generateDataRange(Carbon::now()->subDay('30'), Carbon::now());

        $posts = [];

        foreach($days as $day)
        {
            $posts[] = Post::whereDate('created_at', $day)->where('user_id', Auth::id())->count();
        }

        $chart->dataset('Posts', 'line', $posts);
        $chart->labels($days);
       

        
        return view('author.dashboard', compact('allComments', 'todayComments', 'chart'));

       

    }

    private function generateDataRange( Carbon $start_date, Carbon $end_date )
    {
        $dates = [];

        for($date = $start_date; $date->lte($end_date); $date->addDay())
        {
            $dates[] = $date->format('Y-m-d');
        }

        return $dates;
        
    }
    public function posts()
    {
        return view('author.posts');
    }
    public function comments()
    {
        $posts = Post::where('user_id', Auth::id())->pluck('id')->toArray();
        $comments = Comments::whereIn('post_id', $posts)->get();

        
         return view('author.comments', compact('comments'));
    }

    public function newPost() 
    {
        return view('author.newPost');
    }

    public function CreatePost(CreatePost $request)

    {
        // // Make request CreatePost
         $post = New Post();
         $post->title = $request['title'];
         $post->content = $request['content'];
         $post->user_id = Auth::id();
         $post->save();

         return back()->with('success', 'Post is succefull creted');
    }

    public function deletePost($id) 
    {
        $post = Post::where('id', $id)->where('user_id', Auth::id())->first();
        $comments = Comments::where('post_id', $post->id)->delete();
        $post->delete();

        return back()->with('success', 'Post was deleted');
    }

    public function editPost($id)
    {
        $post = Post::where('id' , $id)->where('user_id', Auth::id())->first();
        
   
         return view('author.editPost', compact('post'));
    }

    public function updatePost($id, UpdatePost $request)
    {
        $post = Post::where('id' , $id)->where('user_id', Auth::id())->first();

        $post->title = $request['title'];
        $post->content = $request['content'];
        $post->save();


        return back()->with('success', 'Succefully update');

    }
}
