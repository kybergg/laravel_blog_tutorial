<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        // Check if user not login and user has admin rights
        if(Auth::check() == false || Auth::user()->$role != true)
        {
            return redirect('/'); 
        }
        return $next($request);
    }
}
