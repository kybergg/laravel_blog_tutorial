<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert(
            [
                ['user_id' => '1', 'title' => 'Posts one', 'content' => 'This is first post'],
                ['user_id' => '1', 'title' => 'Posts two', 'content' => 'This is secound post'],
                ['user_id' => '1', 'title' => 'Posts thre', 'content' => 'This is third post'],
            ]
        );
    }
}
