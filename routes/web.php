<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', 'PublicController@index')->name('index');
Route::get('post/{post}', 'PublicController@singlePost')->name('singlePost');
Route::get('about', 'PublicController@about')->name('about');

Route::get('contact', 'PublicController@contact')->name('contact');
Route::post('contact', 'PublicController@contactPost')->name('contactPost');

Auth::routes();

Route::get('dashboard', 'HomeController@index')->name('dashboard');

// Users

Route::prefix('user')->group(function() {
    // Add comment
    Route::post('newComment', 'UserController@newComment')->name('userNewComment');
    Route::get('dashboard', 'UserController@dashboard')->name('userDashboard');
    Route::get('comments', 'UserController@comments')->name('userComments');
    Route::post('comments/{id}/delete', 'UserController@deleteComments')->name('deleteComments');
    Route::get('profile', 'UserController@profile')->name('userControler');
    Route::post('profile', 'UserController@profilePost')->name('userControlerPost');
}
);

// Author

Route::prefix('author')->group(function() {
    Route::get('dashboard', 'AuthorController@dashboard')->name('authorDashboard');
    Route::get('posts', 'AuthorController@posts')->name('authorPosts');
    Route::get('posts/new', 'AuthorController@newPost')->name('NewPost');
    // Edit posts
    Route::get('posts/{id}/edit', 'AuthorController@editPost')->name('EditPost');
    // Update Post
    Route::post('posts/{id}/edit', 'AuthorController@updatePost')->name('updatePost');
    // Delete post
    Route::post('posts/{id}/delete', 'AuthorController@deletePost')->name('deletePost');
    // Create new post
    Route::post('posts/new', 'AuthorController@createPost')->name('CreatePost');
    Route::get('comments', 'AuthorController@comments')->name('authorComments');
}

);

// Admin

Route::prefix('admin')->group(function() {

    Route::get('dashboard', 'AdminController@dashboard')->name('adminDashboards');
    Route::get('posts', 'AdminController@posts')->name('adminPosts');
    Route::get('posts/{id}/edit', 'AdminController@editPost')->name('adminEditPost');
    // Update Post
    Route::post('posts/{id}/edit', 'AdminController@updatePost')->name('adminUpdatePost');
    // Delete post
    Route::post('posts/{id}/delete', 'AdminController@deletePost')->name('adminDeletePost');
    Route::get('comments', 'AdminController@comments')->name('adminComments');
    //Delete Comments
    Route::post('comments/{id}/delete', 'AdminController@deleteComments')->name('adminDeleteComments');
    Route::get('users', 'AdminController@users')->name('adminUsers');
    Route::post('users/{id}/delete', 'AdminController@userDelete')->name('adminUserDelete');
    Route::get('user/{id}/edit', 'AdminController@editAdminUser')->name('editAdminUser');
    Route::post('user/{id}/edit', 'AdminController@updateAdminUsers')->name('updatetAdminUser');
    // Product
    Route::get('products', 'AdminController@Products')->name('adminProducts');
    Route::get('products/new' , 'AdminController@newProducts')->name('newProduct');
    Route::post('product/new' , 'AdminController@CreateNewProduct')->name('CrearteNewProduct');
    Route::get('product/{id}/edit', 'AdminController@singleProduct' )->name('adminSingleProduct');
    Route::post('product/{id}', 'AdminController@editProduct')->name('EditProduct');
    Route::post('product/{id}/delete', 'AdminController@productDelete')->name('productDelete');
    

});

// Shop

Route::prefix('shop')->group(function() {
    Route::get('/', 'ShopController@index')->name('shopIndex');
    Route::get('product/{id}', 'ShopController@SingleProduct')->name('shop.singleProduct');
    Route::get('product/{id}/order', 'ShopController@productOrder')->name('shop.productOrder');
    Route::get('product/{id}/execute', 'ShopController@execute')->name('shop.execute');
});
